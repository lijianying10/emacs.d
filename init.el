(require 'package)
(add-to-list 'package-archives
                          '("melpa" . "https://melpa.org/packages/") t)
(when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
      (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)

;; hidden toolbar menubar scrollbar
(tool-bar-mode 0)  
(menu-bar-mode 0)  
(scroll-bar-mode 0)  

;; initial powerline
(require 'powerline-evil)
(powerline-default-theme)

;; hightlight-mode
(require 'highlight-symbol)
(global-set-key (kbd "C-4") 'highlight-symbol)
(global-set-key [(control 3)] 'highlight-symbol-next)
(global-set-key [(shift 3)] 'highlight-symbol-prev)
(global-set-key [(meta 3)] 'highlight-symbol-query-replace)

;;(setq server-socket-dir (format "/root/dshare/emacs%d" (user-uid)))


(require 'evil)
(evil-mode t)

(require 'multiple-cursors)
;; smart tab
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4) 

;; go autocomplete
(require 'go-autocomplete)
(require 'auto-complete-config)
(ac-config-default)


;; before save hooks 
(setq exec-path (cons "/usr/local/go/bin" exec-path))
(add-to-list 'exec-path "/go/bin/")

;; add path
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$"
                          ""
                          (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))

;; go mode key hook
(defun my-go-mode-hook ()
  ; Call Gofmt before saving                                                    
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save)
  ; Go oracle
  (load-file "/go/src/golang.org/x/tools/cmd/oracle/oracle.el")
  ; Godef jump key binding                                                      
  (local-set-key (kbd "M-.") 'godef-jump))
(add-hook 'go-mode-hook 'my-go-mode-hook)

;; load theme 
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/molokai-theme")
(load-theme 'molokai t)

(setq inhibit-startup-message t)
(display-time-mode 1)
